/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5612;

import javax.ejb.Stateless;

/**
 *
 * @author martin
 */
@Stateless
public class CalculadoraEJB implements CalculadoraEJBLocal {
    public Double somar(Double num1, Double num2){
        return num1 + num2;
    }

     public Double subtrair(Double num1, Double num2){
        return num1 - num2;
    }
    
}
