package ine5612;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author martin
 */
@ManagedBean(name = "calculadora")
@SessionScoped
public class Calculadora {
    protected String numero1, numero2, resultado;


    @EJB
    CalculadoraEJBLocal calc;
    
    
    /**
     * Creates a new instance of Calculadora
     */
    public Calculadora() {
    }
    
    public String somar(){
        Double soma = calc.somar(new Double(this.numero1), new Double(this.numero2));
        this.resultado = soma.toString();
        return "faces/index.xhtml";
    }

     public String subtrair(){
        Double soma = calc.subtrair(new Double(this.numero1), new Double(this.numero2));
        this.resultado = soma.toString();
        return "faces/index.xhtml";
    }
     
    public String limpar(){
        this.numero1 = "";
        this.numero2 = "";
        this.resultado = "";
        return "faces/index.xhtml";
    }
    
    public String getNumero1() {
        return numero1;
    }

    public void setNumero1(String numero1) {
        this.numero1 = numero1;
    }

    public String getNumero2() {
        return numero2;
    }

    public void setNumero2(String numero2) {
        this.numero2 = numero2;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
}
